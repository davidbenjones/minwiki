if exists("b:current_syntax")
    finish
endif

syntax match minwikiDate "\v^[^\t]+"
highlight link minwikiDate Comment

" This seems really inefficient
syntax match minwikiFiles "\v[\-A-Za-z][^\t]+$"
highlight link minwikiFiles Comment

let b:current_syntax = "minwikilist"
