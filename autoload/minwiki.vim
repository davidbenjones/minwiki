" minwiki.vim - minimal wiki
" Maintainer: Ben Jones <https://www.github.com/davidbenjones>
" Version:    1.0

let s:scriptpath = expand('<sfile>:p:h')
let s:osapath = resolve(s:scriptpath . '/../scripts/openurl.scpt')
let s:osacall = 'osascript ' . s:osapath

function minwiki#Go(...)
	if a:0 == 0
		let l:page_name = s:input('Go to wiki page: ', '')
		if match(l:page_name,'^\s*$') > -1
			echo 'No page given.'
			return
		elseif match(l:page_name,'..*\.md$') == -1
			let l:page_name = l:page_name . '.md'
		endif
	else
		let l:page_name = a:1
	endif

	call s:openpage(l:page_name)
endfunction

function s:openpage(page)
	let l:type = s:urltype(a:page)

	if l:type == 'web'
		let l:call = s:osacall . " '" . a:page . "'"
		silent! call system(l:call)
		echo "Link opened in web browser."
		return
	elseif s:urltype(a:page) !=? 'wiki'
		echo 'Cannot follow link.'
		return
	endif

	if s:iswiki()
		update
	endif

	if type(a:page) == v:t_number
		let l:ft = &syntax
		if (@% != "" || l:ft == 'minwikilist')
			enew
		endif
		call s:openindex()
	else
		let l:page_path = g:minwiki_path . "/" . a:page
		exe "edit " . l:page_path
	endif

	if (!exists("w:minwiki_history"))
		let w:minwiki_history = []
	endif

	call add(w:minwiki_history, a:page)
endfunction

function s:openindex()
	silent! 0read !notes
	norm G
	norm "_dd
	norm gg
	setlocal colorcolumn=
	setlocal modifiable!
	setlocal readonly
	setlocal buftype=nowrite
	setlocal bufhidden=delete
	setlocal noswapfile
	setlocal buflisted!
	setlocal nowrap

	setlocal filetype=minwiki
	setlocal syntax=minwikilist
endfunction

function minwiki#AutocompletePage(A,L,P)
	let l:searchtext = trim(a:A)
	" NOTE: To speed things up, maybe only apply this to the
	" last component of a path instead of to the whole thing.
	let l:searchtext = join(split(l:searchtext, '\zs'), '*')
	let l:results = glob(g:minwiki_path . '/**' . l:searchtext . '**', 0, 1)
	let l:results = map(l:results, "fnamemodify(v:val, ':p')")
	let l:path = s:regexmagicescape(g:minwiki_path . '/', '')
	let l:pat  = '\v^' . l:path
	let l:results = map(l:results, "substitute(v:val, l:pat, '', '')")
	return l:results
endfunction

function s:input(prompt,default)
	call inputsave()
	let l:input = input(a:prompt, a:default, 'customlist,minwiki#AutocompletePage')
	call inputrestore()
	redraw
	return trim(l:input)
endfunction

function s:urltype(url)
	if type(a:url) == v:t_number
		return 'wiki'
	elseif match(a:url, '^https\?://') != -1
		return 'web'
	elseif match(a:url, '.*\.md$') != -1
		return 'wiki'
	elseif match(a:url,'^\s*$') > -1
		return 'blank'
	endif
	return 'other'
endfunction

" escape for magic mode
function s:regexmagicescape(string, other_characters)
	" use a:other_characters to add delimiter
	return escape(a:string, '*^$.&~\' . a:other_characters)
endfunction

function s:getcurrentchar()
	return strcharpart(strpart(getline('.'), col('.') - 1), 0, 1)
endfunction

function s:get_visual_selection()
	let l:reg = getreg("a")
	let l:regtype = getregtype("a")

	normal! "ay
	let l:text = getreg("a")

	call setreg("a", l:reg, l:regtype)
	return l:text
endfunction

function s:startpos()
	let l:pos = getpos('.') " [bufnum, line, col, off?]
	let l:char = s:getcurrentchar()

	" get start position
	call search("\[", "bcW")
	let l:spos = getpos('.')
	if (l:pos == l:spos && l:char != '[')
		return [0, 0, 0, 0]
	endif

	" we are on '['; check previous
	" Technically an optional space is allowed between the link text
	" and the link, but I don't believe in that.

	let l:start = getpos('.')

	normal! h
	if s:getcurrentchar() == ']'
		" already in link, move to start
		call search('\v\[', 'bcW')
		if s:getcurrentchar() != '['
			return [0, 0, 0, 0]
		endif
		let l:start = getpos('.')
	endif

	call setpos('.', l:pos)
	return l:start
endfunction

function s:midpos(startpos)
	let l:pos = getpos('.') " [bufnum, line, col, off?]
	let l:char = s:getcurrentchar()

	call setpos('.', a:startpos)

	" l:type
	" 	no = 0
	" 	]( = 2
	" 	][ = 3
	" 	]: = 4
	" 	]  = 5
	let l:type = search('\v(] ?\()|(] ?\[)|(]: ?)|(])', 'peW')
	let l:mid = getpos('.')

	if (l:type == 0)
		return [l:mid, 0]
	endif

	call setpos('.', l:pos)
	return [l:mid, l:type]
endfunction

function s:endpos(midpos, type)
	let l:pos = getpos('.') " [bufnum, line, col, off?]
	let l:char = s:getcurrentchar()

	call setpos('.', a:midpos)

	if a:type == 2
		call search('\v\)','W')
	elseif a:type == 3
		call search('\v]', 'W')
	elseif a:type == 4
		" this isn't quite right according to markdown, but it's
		" good enough for what I use
		normal! $
	endif
	let l:end = getpos('.')

	call setpos('.', l:pos)
	return l:end
endfunction

function s:betweenpos(pos, start, end)
	" lists [bufnum, line, col, off]

	if (a:pos[1] < a:start[1] || a:pos[1] > a:end[1])
		return 0
	elseif (a:pos[2] < a:start[2] || a:pos[2] > a:end[2])
		return 0
	endif

	return 1

endfunction

" if the cursor is over a link, return the link URL
function s:getlink()
	let l:pos = getpos('.')
	let l:null = [0, 0, 0, 0]

	let l:start = s:startpos()
	if l:start == l:null
		return ''
	endif

	let [l:mid, l:type] = s:midpos(l:start)

	if l:type == 5
		" doesn't matter if it's between; it's not a full link
		return ''
	endif

	let l:end = s:endpos(l:mid, l:type)
	let l:between = s:betweenpos(l:pos, l:start, l:end)

	if l:between == 0
		" there's a link, but the cursor isn't on it
		return ''
	endif

	call setpos('.', l:mid)
	normal! lv
	call setpos('.', l:end)
	if l:type != 4
		normal! h
	endif
	let l:text = s:get_visual_selection()

	if l:type == 3
		" move to reference
		let l:text = s:regexmagicescape(l:text, '')
		let l:mtchc = search('\v^\[' . l:text . ']: +', 'e')
		if l:mtchc == 0
			return ''
		endif
		" still not totally correct markdown
		normal! lv$
		let l:text = s:get_visual_selection()
	endif

	call setpos('.', l:pos)

	return s:cleanlink(l:text)
endfunction

function s:addbrackets(pos)
	let l:pos = getpos('.')
	call setpos('.', l:pos)
	let l:char = s:getcurrentchar()
	if trim(l:char) == ''
		return 0
	endif
	let l:word = expand('<cWORD>')
	let l:text = '[' . l:word . ']'
	exec 'normal! ciw' . l:text
	call setpos('.', l:pos)
endfunction

function s:createlink()
	let l:pos = getpos('.')
	let l:null = [0, 0, 0, 0]

	let l:start = s:startpos()
	if (l:start == l:null)
		call s:addbrackets(l:pos)
		return 0
	endif

	let [l:mid, l:type] = s:midpos(l:start)
	let l:end = s:endpos(l:mid, l:type)
	let l:between = s:betweenpos(l:pos, l:start, l:end)

	if l:between == 0
		call s:addbrackets(l:pos)
		return 0
	endif

	call setpos('.', l:pos)
	normal! vi[
	let l:text = s:get_visual_selection()
	let l:text = s:cleanlink(l:text)

	call setpos('.', l:end)
	exec 'normal! a(' . l:text . '.md)'

	echo 'new link: ' . l:text . '.md'
endfunction

function s:cleanlink(str)
	let l:result = substitute(trim(a:str), '\s\+', '-', 'g')
	let l:result = substitute(l:result, '\n', '-', 'g')
	return l:result
endfunction

function minwiki#Enter()
	let l:pos = getpos('.')
	let l:char = s:getcurrentchar()
	let l:ft = &syntax
	if l:ft == 'minwikilist'
		norm $
		let link_text = expand('<cfile>')
	else
		let link_text = s:getlink()
	endif

	if link_text == ''
		if l:char != ' '
			call s:createlink()
		endif
	else
		call minwiki#Go(link_text)
	endif
	call setpos('.', l:pos)
endfunction

function minwiki#PrevPage()
	if len(w:minwiki_history) > 1
		let prev_page = w:minwiki_history[-2]
		let w:minwiki_history = w:minwiki_history[0:-3]
		call minwiki#Go(prev_page)
	else
		echo "No previous pages."
	endif
endfunction

function minwiki#NextLink(count)
	let i = a:count
	while i
		call search('\v\[[^\]]*](\(|\[|:)')
		let i = i - 1
	endwhile
endfunction

function minwiki#PrevLink(count)
	let i = a:count
	while i
		call search('\v\[[^\]]*](\(|\[|:)', 'b')
		let i = i - 1
	endwhile
endfunction

function s:iswiki()
	let l:ft = &filetype
	if l:ft == 'minwikilist'
		return 1
	elseif expand('%:p:h') ==? fnamemodify(g:minwiki_path, ':p:h')
		return 1
	else
		return 0
	endif
endfunction
